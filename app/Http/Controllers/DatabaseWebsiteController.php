<?php

namespace App\Http\Controllers;

use App\Models\App;
use App\Models\Sumber;
use App\Models\Website;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\DB;


class DatabaseWebsiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('tb_app')->join('tb_website', 'tb_app.id' , '=', 'tb_website.id_app')->where('tb_website.status', '>=', '0')->get();
        // $data = Website::where('status', '>=', '0')->get();
        return view('dashboard.tabledatawebsite', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $datamaster = App::where('status', '>=', '0')->get();
        $datasumber = Sumber::where('status', '>=', '0')->get();
        return view('dashboard.tambahdatawebsite', compact('datamaster', 'datasumber'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Website::create([
            'judul' => $request->nama_laporan,
            'kategori' => $request->id_grup,
            'id_app' => $request->id_master,
            'pemilik' => $request->pemilik,
            'kode' => $request->kode_laporan,
            'url' => $request->sumber_data,
        ]);

        Alert::success('Success Message', 'Success Save');
        $data = DB::table('tb_app')->join('tb_website', 'tb_app.id' , '=', 'tb_website.id_app')->where('tb_website.status', '>=', '0')->get();
        return redirect()->route('datawebsite')->with(['data']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Website::where('id', $id)->first();
        $datamaster = App::where('status', '>=', '0')->get();
        $datasumber = Sumber::where('status', '>=', '0')->get();
        return view('dashboard.editdatawebsite', compact('data', 'datamaster', 'datasumber'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Website::where('id', $id)->update([
            'judul' => $request->nama_laporan,
            'kategori' => $request->id_grup,
            'id_app' => $request->id_master,
            'kode' => $request->kode_laporan,
            'url' => $request->sumber_data,
        ]);

        Alert::success('Success Message', 'Success Edit');
        $data = DB::table('tb_app')->join('tb_website', 'tb_app.id' , '=', 'tb_website.id_app')->where('tb_website.status', '>=', '0')->get();
        return redirect()->route('datawebsite')->with(['data']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Website::where('id', $id)->update([
            'status' => '-1',
        ]);

        Alert::success('Success Message', 'Success Delete');
        $data = DB::table('tb_app')->join('tb_website', 'tb_app.id' , '=', 'tb_website.id_app')->where('tb_website.status', '>=', '0')->get();
        return redirect()->route('datawebsite')->with(['data']);
    }
}
