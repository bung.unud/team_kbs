<?php

namespace App\Http\Controllers;

use App\Models\App;
use App\Models\Sumber;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class SumberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = DB::table('tb_sumber')->join('tb_app', 'tb_sumber.id_app', '=', 'tb_app.id')->where('tb_sumber.status', '>=', '0')->get();
        return view('dashboard.tablesumber', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $datamaster = App::where('status', '>=', '0')->get();
        return view('dashboard.tambahdatasumber', compact('datamaster'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Sumber::create([
            'id_app' => $request->id_master,
            'endpoint' => $request->endpoint,
            'api' => $request->api,
            
        ]);

        Alert::success('Success Message', 'Success Save');
        $data = DB::table('tb_sumber')->join('tb_app', 'tb_sumber.id_app', '=', 'tb_app.id')->where('tb_sumber.status', '>=', '0')->get();
        return redirect()->route('datasumber')->with(['data']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Sumber::where('id', $id)->first();
        $datamaster = App::where('status', '>=', '0')->get();
        return view('dashboard.editdatasumber', compact('data', 'datamaster'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Sumber::where('id', $id)->update([
            'id_app' => $request->id_master,
            'endpoint' => $request->endpoint,
            'api' => $request->api,
        ]);

        Alert::success('Success Message', 'Success Update');
        $data = DB::table('tb_sumber')->join('tb_app', 'tb_sumber.id_app', '=', 'tb_app.id')->where('tb_sumber.status', '>=', '0')->get();
        return redirect()->route('datasumber')->with(['data']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Sumber::where('id', $id)->update([
            'status' => '-1',
        ]);

        Alert::success('Success Message', 'Success Delete');
        $data = DB::table('tb_sumber')->join('tb_app', 'tb_sumber.id_app', '=', 'tb_app.id')->where('tb_sumber.status', '>=', '0')->get();
        return redirect()->route('datasumber')->with(['data']);
    }
}
