@extends('dashboard.main')

@section('subjudul')
<span class="judul-dashboard">Database Website</span>
<span class="judul-dashboard px-3">|</span>
<span class="subjudul-dashboard">Tabel Database Website</span>

@endsection

@section('content')
<div id="wrapper" style="background-color: black;">
  <ul class="navbar-nav sidebar sidebar-dark accordion" style="background-color: white;" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="row sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="p-0 sidebar-brand-icon">
          <i class="bi bi-person-circle"></i>
        </div>
        <div class="p-0 sidebar-brand-text">{{ Auth::user()->name  }}</div>
        <div class="p-0 sidebar-brand-text">Admin</div>
    </a>


    <!-- Nav Item - Dashboard -->
    <li class="nav-item ">
        <a class="nav-link" href='{{ route('dashboard') }}'>
            <i class="bi bi-speedometer2 bi-color"></i>
            <span>Dashboard</span>
        </a>
    </li>

    <li class="nav-item active">
        <a class="nav-link collapsed" href='{{ route('datawebsite') }}' data-target="#collapseTwo"
            aria-expanded="true" aria-controls="collapseTwo">
            <i class="bi bi-clipboard-data bi-color"></i>
            <span>Database Website</span>
        </a>
    </li>
    <li class="nav-item">
      <a class="nav-link collapsed" href='{{ url('/tambah-data') }}' data-target="#collapseTwo"
          aria-expanded="true" aria-controls="collapseTwo">
          <i class="bi bi-plus-circle-fill bi-color"></i>
          <span>Tambah Database Website</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link collapsed" href='{{ url('/tabledataapp') }}' data-target="#collapseTwo"
          aria-expanded="true" aria-controls="collapseTwo">
          <i class="bi bi-table bi-color"></i>
          <span>Tabel Aplikasi dan OPD</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link collapsed" href='{{ url('/tambah-data-app') }}' data-target="#collapseTwo"
          aria-expanded="true" aria-controls="collapseTwo">
          <i class="bi bi-plus-circle-fill bi-color"></i>
          <span>Tambah Master Aplikasi</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-target="#collapseTwo"
          aria-expanded="true" aria-controls="collapseTwo">
          <i class="bi bi-gear-fill bi-color"></i>
          <span>Setting</span>
      </a>
    </li>
    <li class="nav-item">
        <form method="POST" action="{{ route('logout') }}">
            @csrf
            <a class="nav-link collapsed" href="route('logout')" onclick="event.preventDefault(); this.closest('form').submit();" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
              <i class="bi bi-box-arrow-left bi-color"></i>
              <span>Keluar</span>
            </a>
          </form>
    </li>
  </ul>
</div>
@endsection

@section('content2')

<div>
  <form class="searchtbl">
    <button type="submit" class="search-button"><i class="bi bi-search icon-search2"></i></button>
    <input class="search-container" type="search" placeholder="Cari dalam tabel" aria-label="Search">
    <button type="button" class="tambahdata" onclick="location.href='{{ url('/tambah-data') }}'">
      <p>+ Data Website</p>
    </button>
  </form>
</div>

<div class="bdr table"><br>
  <table class="table-hover table-responsive">
    <thead>
      <tr class="thead">
        <th>No.</th>
        <th>Judul</th>
        <th>Pemilik Aplikasi</th>
        <th class="th">Tindakan</th>
      </tr>
    </thead>

    @php
    $no=1;
    @endphp
    @foreach ( $data as $item)
    <tr>
    <td>{{ $no++ }}</td>
    <td>{{ $item->judul }}</td>
    <td>{{ $item->pemilik }}</td>
    <td>
        <a href="{{ url('data/' . $item->id) }}" class="btn btn-warning">Edit</a>
        {{-- <button type="submit" class="search-button2"><i class="bi bi-pencil-square"></i></i></button> --}}
        <form action="{{ url('destroy/'.$item->id) }}" method="POST" class="d-inline">
          @csrf
          @method('DELETE')
          <button class="btn btn-danger">Hapus</i></button>
        </form>
    </td>
    </tr>
    @endforeach 
    
    {{-- <tr>
      <td>01.</td>
      <td style="text-align: left">Syarat Pembuatan KTP - Website Portal Resmi Pemerintah Kota Denpasar</td>
      <td>Disdukcapil</td>
      <td>
        <button type="submit" class="search-button2"><i class="bi bi-pencil-square"></i></i></button>
        <button type="submit" class="search-button3"><i class="bi bi-trash3 icon-delete"></i></i></button>
      </td>
    </tr>
    <tr>
      <td>02.</td>
      <td style="text-align: left">Layanan - Dinas Kependudukan dan Pencatatan Sipil</td>
      <td>Disdukcapil</td>
      <td>
        <button type="submit" class="search-button2"><i class="bi bi-pencil-square"></i></i></button>
        <button type="submit" class="search-button3"><i class="bi bi-trash3 icon-delete"></i></i></button>
      </td>
    </tr>
    <tr>
      <td>03.</td>
      <td style="text-align: left">Dinas DukCapil - Pemerintah Kabupaten Jembrana</td>
      <td>Disdukcapil</td>
      <td>
        <button type="submit" class="search-button2"><i class="bi bi-pencil-square"></i></i></button>
        <button type="submit" class="search-button3"><i class="bi bi-trash3 icon-delete"></i></i></button>
      </td>
    </tr>
    <tr>
      <td>04.</td>
      <td style="text-align: left">Pelayanan Kartu Tanda Penduduk - Website Resmi Dinas Kabupaten Badung</td>
      <td>Disdukcapil</td>
      
      <td>
        <button type="submit" class="search-button2"><i class="bi bi-pencil-square"></i></i></button>
        <button type="submit" class="search-button3"><i class="bi bi-trash3 icon-delete"></i></i></button>
      </td>
    </tr> --}}
    
  </table>
</div>


@endsection