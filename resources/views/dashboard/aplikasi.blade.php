@extends('dashboard.main')

<script src="https://cdn.jsdelivr.net/npm/apexcharts"></script>

@section('subjudul')
<span class="judul-dashboard">Aplikasi dan OPD</span>
<span class="judul-dashboard px-3">|</span>
<span class="subjudul-dashboard">Tabel Aplikasi dan OPD</span>

@endsection

@section('content')
<div id="wrapper" style="background-color: black;">
  <ul class="navbar-nav sidebar sidebar-dark accordion" style="background-color: white;" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="row sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="p-0 sidebar-brand-icon">
          <i class="bi bi-person-circle"></i>
        </div>
        <div class="p-0 sidebar-brand-text">Wijaya Putra</div>
        <div class="p-0 sidebar-brand-text">Admin</div>
    </a>


    <!-- Nav Item - Dashboard -->
    <li class="nav-item ">
        <a class="nav-link" href='{{ route('dashboard') }}'>
            <i class="bi bi-speedometer2 bi-color"></i>
            <span>Dashboard</span>
        </a>
    </li>

    <li class="nav-item ">
        <a class="nav-link collapsed" href='{{ route('datawebsite') }}' data-target="#collapseTwo"
            aria-expanded="true" aria-controls="collapseTwo">
            <i class="bi bi-clipboard-data bi-color"></i>
            <span>Database Website</span>
        </a>
    </li>
    <li class="nav-item">
      <a class="nav-link collapsed" href='{{ route('tambahdatawebsite') }}' data-target="#collapseTwo"
          aria-expanded="true" aria-controls="collapseTwo">
          <i class="bi bi-plus-circle-fill bi-color"></i>
          <span>Tambah Database Website</span>
      </a>
    </li>
    <li class="nav-item active">
      <a class="nav-link collapsed" href='#' data-target="#collapseTwo"
          aria-expanded="true" aria-controls="collapseTwo">
          <i class="bi bi-table bi-color"></i>
          <span>Tabel Aplikasi dan OPD</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link collapsed" href='{{ route('tambahmaster') }}' data-target="#collapseTwo"
          aria-expanded="true" aria-controls="collapseTwo">
          <i class="bi bi-plus-circle-fill bi-color"></i>
          <span>Tambah Master Aplikasi</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-target="#collapseTwo"
          aria-expanded="true" aria-controls="collapseTwo">
          <i class="bi bi-gear-fill bi-color"></i>
          <span>Setting</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-target="#collapseTwo"
          aria-expanded="true" aria-controls="collapseTwo">
          <i class="bi bi-box-arrow-left bi-color"></i>
          <span>Keluar</span>
      </a>
    </li>
  </ul>
</div>

@endsection

@section('content2')

<div>
  <form class="searchtbl">
    <button type="submit" class="search-button"><i class="bi bi-search icon-search2"></i></button>
    <input class="search-container" type="search" placeholder="Cari dalam tabel" aria-label="Search">
    <button type="button" class="tambahdata" onclick="location.href='{{ route('tdw') }}'">
      <p>+ Master Aplikasi</p>
    </button>
  </form>
</div>

<div class="bdr table">
  <table class="table-hover table-responsive">
    <thead>
      <tr class="thead">
        <th>No.</th>
        <th>Nama</th>
        <th>Pemilik Aplikasi</th>
        <th>URL Aplikasi</th>
        <th class="th">Tindakan</th>
        <th>Status</th>
      </tr>
    </thead>
    <tr>
      <td>01.</td>
      <td style="text-align: left">Sikuat</td>
      <td>DISKOMINFOS Provinsi Bali</td>
      <td>
      </td>
      <td>
        <button type="submit" class="search-button2" onclick="location.href='{{ route('embed') }}'"><i class="bi bi-pencil-square"></i></i></button>
        <button type="submit" class="search-button3"><i class="bi bi-trash3 icon-delete"></i></i></button>
      </td>
      <td>
        <div class="form-switch">
          <input class="form-check-input" type="checkbox" role="switch" id="flexSwitchCheckDefault">
        </div>
      </td>
    </tr>
    <tr>
      <td>02.</td>
      <td style="text-align: left">Love Bali</td>
      <td>DISKOMINFOS Provinsi Bali</td>
      <td>
      </td>
      <td>
        <button type="submit" class="search-button2"><i class="bi bi-pencil-square"></i></i></button>
        <button type="submit" class="search-button3"><i class="bi bi-trash3 icon-delete"></i></i></button>
      </td>
      <td>
        <div class="form-switch">
          <input class="form-check-input" type="checkbox" role="switch" id="flexSwitchCheckDefault">
        </div>
      </td>
    </tr>
    <tr>
      <td>03.</td>
      <td style="text-align: left">Bali Media Center</td>
      <td>DISKOMINFOS Provinsi Bali</td>
      <td>
      </td>
      <td>
        <button type="submit" class="search-button2"><i class="bi bi-pencil-square"></i></i></button>
        <button type="submit" class="search-button3"><i class="bi bi-trash3 icon-delete"></i></i></button>
      </td>
      <td>
        <div class="form-switch">
          <input class="form-check-input" type="checkbox" role="switch" id="flexSwitchCheckDefault">
        </div>
      </td>
    </tr>
    <tr>
      <td>04.</td>
      <td style="text-align: left">E-Office</td>
      <td>DISKOMINFOS Provinsi Bali</td>
      <td>
      </td>
      <td>
        <button type="submit" class="search-button2"><i class="bi bi-pencil-square"></i></i></button>
        <button type="submit" class="search-button3"><i class="bi bi-trash3 icon-delete"></i></i></button>
      </td>
      <td>
        <div class="form-switch">
          <input class="form-check-input" type="checkbox" role="switch" id="flexSwitchCheckDefault">
        </div>
      </td>
    </tr>
    <tr>
      <td>05.</td>
      <td style="text-align: left">E-Office</td>
      <td>DISKOMINFOS Provinsi Bali</td>
      <td>
      </td>
      <td>
        <button type="submit" class="search-button2"><i class="bi bi-pencil-square"></i></i></button>
        <button type="submit" class="search-button3"><i class="bi bi-trash3 icon-delete"></i></i></button>
      </td>
      <td>
        <div class="form-switch">
          <input class="form-check-input" type="checkbox" role="switch" id="flexSwitchCheckDefault">
        </div>
      </td>
    </tr>
    <tr>
      <td>06.</td>
      <td style="text-align: left">E-Office</td>
      <td>DISKOMINFOS Provinsi Bali</td>
      <td>
      </td>
      <td>
        <button type="submit" class="search-button2"><i class="bi bi-pencil-square"></i></i></button>
        <button type="submit" class="search-button3"><i class="bi bi-trash3 icon-delete"></i></i></button>
      </td>
      <td>
        <div class="form-switch">
          <input class="form-check-input" type="checkbox" role="switch" id="flexSwitchCheckDefault">
        </div>
      </td>
    </tr>
    
  </table>
</div>


@endsection