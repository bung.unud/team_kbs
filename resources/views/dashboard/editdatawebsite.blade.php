@extends('dashboard.main')

@section('subjudul')
<span class="judul-dashboard">Database Website</span>
<span class="judul-dashboard px-3">|</span>
<span class="subjudul-dashboard">Edit Database Website</span>

@endsection

@section('content')
<div id="wrapper" style="background-color: black;">
  <ul class="navbar-nav sidebar sidebar-dark accordion" style="background-color: white;" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="row sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="p-0 sidebar-brand-icon">
          <i class="bi bi-person-circle"></i>
        </div>
        <div class="p-0 sidebar-brand-text">{{ Auth::user()->name  }}</div>
        <div class="p-0 sidebar-brand-text">Admin</div>
    </a>


    <!-- Nav Item - Dashboard -->
    <li class="nav-item ">
        <a class="nav-link" href='{{ route('dashboard') }}'>
            <i class="bi bi-speedometer2 bi-color"></i>
            <span>Dashboard</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href='{{ route('datawebsite') }}' data-target="#collapseTwo"
            aria-expanded="true" aria-controls="collapseTwo">
            <i class="bi bi-clipboard-data bi-color"></i>
            <span>Database Website</span>
        </a>
    </li>
    <li class="nav-item active">
      <a class="nav-link collapsed" href='{{ url('/tambah-data') }}' data-target="#collapseTwo"
          aria-expanded="true" aria-controls="collapseTwo">
          <i class="bi bi-plus-circle-fill bi-color"></i>
          <span>Tambah Database Website</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link collapsed" href='{{ url('/tabledataapp') }}' data-target="#collapseTwo"
          aria-expanded="true" aria-controls="collapseTwo">
          <i class="bi bi-table bi-color"></i>
          <span>Tabel Aplikasi dan OPD</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link collapsed" href='{{ url('/tambah-data-app') }}' data-target="#collapseTwo"
          aria-expanded="true" aria-controls="collapseTwo">
          <i class="bi bi-plus-circle-fill bi-color"></i>
          <span>Tambah Master Aplikasi</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-target="#collapseTwo"
          aria-expanded="true" aria-controls="collapseTwo">
          <i class="bi bi-gear-fill bi-color"></i>
          <span>Setting</span>
      </a>
    </li>
    <li class="nav-item">
        <form method="POST" action="{{ route('logout') }}">
            @csrf
            <a class="nav-link collapsed" href="route('logout')" onclick="event.preventDefault(); this.closest('form').submit();" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
              <i class="bi bi-box-arrow-left bi-color"></i>
              <span>Keluar</span>
            </a>
          </form>
    </li>
  </ul>
</div>

@endsection

@section('content2')
          
              <form action="{{ url('data') }}/{{ $data->id }}" method="POST" class="form">
                @csrf
                <div class="row mb-3">
                  <label for="nama_laporan" class="col-sm-2 col-form-label text-dark judul">Nama Laporan :</label>
                  <div class="col-sm-10">
                    <input type="text" name="nama_laporan" value="{{ $data->judul }}" class="form-control" placeholder="Nama Laporan">
                  </div>
                </div>

                <div class="row mb-3">
                  <label for="id_grup" class="col-sm-2 col-form-label text-dark judul">Grup Laporan :</label>
                  <div class="col-sm-10">
                    <select name="id_grup" class="form-select">
                        <option value="fms" >FMS</option>
                        <option value="url" >URL</option>
                        {{-- @foreach ($grup as $item)
                          <option value="{{ $item->id }}"> {{ $item->nama_grup }}</option>
                        @endforeach --}}
                    </select>
                  </div>
                </div>

                <div class="row mb-3">
                    <label for="id_master" class="col-sm-2 col-form-label text-dark judul">Master Aplikasi :</label>
                    <div class="col-sm-10">
                      <select name="id_master" class="form-select">
                          @foreach ($datamaster as $item)
                            <option value="{{ $item->id }}"> {{ $item->nama }} - {{ $item->pemilik }}</option>
                          @endforeach
                      </select>
                    </div>
                  </div>

                {{-- <div class="row mb-3">
                  <label for="inputEmail3" class="col-sm-2 col-form-label text-dark judul">Pemilik Aplikasi :</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control input" disabled placeholder="Pemilik Aplikasi">
                  </div>
                </div> --}}

                {{-- <div class="row mb-3">
                  <label for="id_opd" class="col-sm-2 col-form-label text-dark judul">Pemilik Aplikasi :</label>
                  <div class="col-sm-10">
                    <input type="text" value="Bambang" name="pemilik" class="form-control input" readonly>


                    <select name="id_opd" id="">
                      <option value="" disabled selected hidden>Pilih Pemilik Aplikasi</option>
                        @foreach ($opd as $item)
                          <option value="{{ $item->id }}"> {{ $item->nama_opd }}</option>
                        @endforeach
                    </select>
                  </div>
                </div> --}}

                <div class="row mb-3">
                  <label for="kode_laporan" class="col-sm-2 col-form-label text-dark judul">Kode Laporan :</label>
                  <div class="col-sm-10">
                    <input type="text" name="kode_laporan" value="{{ $data->kode }}" class="form-control" placeholder="Kode Laporan">
                  </div>
                </div>

                <div class="row mb-3">
                  <label for="id_sumber" class="col-sm-2 col-form-label text-dark judul">Sumber Data :</label>
                  <div class="col-sm-10">
                    <select name="sumber_data" class="form-select">
                      @foreach ($datasumber as $item)
                        <option value="{{ $item->id }}"> {{ $item->endpoint }} | {{ $item->api }}</option>
                      @endforeach
                  </select>
                  </div>
                </div>

                <div class="button">
                  <button type="reset" class="btn btn-primary">Batal</button>
                  <button type="submit" class="btn btn-primary" id="simpan" >Edit</button>
                </div>
                
              </form>
            </div>
          </div>
@endsection