@extends('dashboard.main')

@section('subjudul')
<span class="judul-dashboard">Database Sumber</span>
<span class="judul-dashboard px-3">|</span>
<span class="subjudul-dashboard">Tambah Database Sumber</span>

@endsection

@section('content')
<div id="wrapper" style="background-color: black;">
  <ul class="navbar-nav sidebar sidebar-dark accordion" style="background-color: white;" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="row sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <div class="p-0 sidebar-brand-icon">
          <i class="bi bi-person-circle"></i>
        </div>
        <div class="p-0 sidebar-brand-text">{{ Auth::user()->name  }}</div>
        <div class="p-0 sidebar-brand-text">Admin</div>
    </a>


    <!-- Nav Item - Dashboard -->
    <li class="nav-item ">
        <a class="nav-link" href='{{ route('dashboard') }}'>
            <i class="bi bi-speedometer2 bi-color"></i>
            <span>Dashboard</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link collapsed" href='{{ route('datawebsite') }}' data-target="#collapseTwo"
            aria-expanded="true" aria-controls="collapseTwo">
            <i class="bi bi-clipboard-data bi-color"></i>
            <span>Database Website</span>
        </a>
    </li>
    <li class="nav-item active">
      <a class="nav-link collapsed" href='{{ url('/tambah-data') }}' data-target="#collapseTwo"
          aria-expanded="true" aria-controls="collapseTwo">
          <i class="bi bi-plus-circle-fill bi-color"></i>
          <span>Tambah Database Website</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link collapsed" href='{{ url('/tabledataapp') }}' data-target="#collapseTwo"
          aria-expanded="true" aria-controls="collapseTwo">
          <i class="bi bi-table bi-color"></i>
          <span>Tabel Aplikasi dan OPD</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link collapsed" href='{{ url('/tambah-data-app') }}' data-target="#collapseTwo"
          aria-expanded="true" aria-controls="collapseTwo">
          <i class="bi bi-plus-circle-fill bi-color"></i>
          <span>Tambah Master Aplikasi</span>
      </a>
    </li>
    <li class="nav-item">
      <a class="nav-link collapsed" href="#" data-target="#collapseTwo"
          aria-expanded="true" aria-controls="collapseTwo">
          <i class="bi bi-gear-fill bi-color"></i>
          <span>Setting</span>
      </a>
    </li>
    <li class="nav-item">
        <form method="POST" action="{{ route('logout') }}">
            @csrf
            <a class="nav-link collapsed" href="route('logout')" onclick="event.preventDefault(); this.closest('form').submit();" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
              <i class="bi bi-box-arrow-left bi-color"></i>
              <span>Keluar</span>
            </a>
          </form>
    </li>
  </ul>
</div>

@endsection

@section('content2')
          
              <form action="{{ url('tambah-data-sumber') }}" method="POST" class="form">
                @csrf

                <div class="row mb-3">
                  <label for="id_master" class="col-sm-2 col-form-label text-dark judul">Master Aplikasi :</label>
                  <div class="col-sm-10">
                    <select name="id_master" id="" class="form-select">
                      <option value="" disabled selected hidden>Pilih Master Aplikasi</option>
                        @foreach ($datamaster as $item)
                          <option value="{{ $item->id }}"> {{ $item->nama }} - {{ $item->pemilik }}</option>
                        @endforeach
                    </select>
                  </div>
                </div>

                {{-- <div class="row mb-3">
                  <label for="inputEmail3" class="col-sm-2 col-form-label text-dark judul">Pemilik Aplikasi :</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control input" disabled placeholder="Pemilik Aplikasi">
                  </div>
                </div> --}}

                {{-- <div class="row mb-3">
                  <label for="id_opd" class="col-sm-2 col-form-label text-dark judul">Pemilik Aplikasi :</label>
                  <div class="col-sm-10">
                    <input type="text" value="Bambang" name="pemilik" class="form-control input" readonly>


                    <select name="id_opd" id="">
                      <option value="" disabled selected hidden>Pilih Pemilik Aplikasi</option>
                        @foreach ($opd as $item)
                          <option value="{{ $item->id }}"> {{ $item->nama_opd }}</option>
                        @endforeach
                    </select>
                  </div>
                </div> --}}

                <div class="row mb-3">
                  <label for="kode_laporan" class="col-sm-2 col-form-label text-dark judul">End Point :</label>
                  <div class="col-sm-10">
                    <input type="text" name="endpoint" class="form-control" placeholder="End Point">
                  </div>
                </div>

                <div class="row mb-3">
                  <label for="id_sumber" class="col-sm-2 col-form-label text-dark judul">URL API :</label>
                  <div class="col-sm-10">
                    <input type="text" name="api" class="form-control" placeholder="Sumber Data">
                    </select>
                  </div>
                </div>

                <div class="button">
                  <button style="background-color:grey" type="reset" class="btn btn-primary">Batal</button>
                  <button type="submit" class="btn btn-primary" id="simpan" >Simpan</button>
                </div>
                
              </form>
            </div>
          </div>
@endsection