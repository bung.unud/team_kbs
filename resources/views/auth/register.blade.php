<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
        <!-- Custome Style -->
        <link rel="stylesheet" type="text/css" href="{{ asset('/css/custome-style.css') }}">
    
        <!-- Logo title bar -->
        <link rel="icon" href="{{ asset('/assets/img/logo-bali.png') }}" type="image/x-icon">
    
        <title>Knowledge Base Systems</title>
    </head>
    <style>
        .regis p{
            font-style: normal;
            font-weight: 700;
            font-size: 55px;
            line-height: 82px;

            color: #00415A;

            text-shadow: 2px 2px 4px rgba(0, 65, 90, 0.56);
        }
        .input-regis{
            width: 364px;
            height: 46px;
            left: 898px;
            top: 396px;
            color: white;
            background: rgba(0, 65, 90, 0.56);
            border-radius: 10px;
        }
        .input-regis::-webkit-input-placeholder{
	        color: white;
        }
        .btn-regis{
            width: 150px;
            height: 48px;
            font-style: normal;
            font-weight: 600;
            font-size: 20px;
            text-transform: uppercase;
            color: #E5E5E5;
            background: #00415A;
            border-radius: 10px;
            filter: drop-shadow(4px 4px 4px rgba(0, 0, 0, 0.25));
        }
        .btn-regis:hover{
            color: #E5E5E5;
            background-color: #55c449;
        }
        .judul p{
            color: white;
            font-style: normal;
        }
        .judul1{
            font-weight: 650;
            font-size: 22px;
            line-height: 34px;
            text-shadow: 2px 2px 4px #C4C4C4;
        }
        .judul2{
            font-weight: 700;
            font-size: 50px;
            line-height: 64px;
            text-shadow: 2px 2px 4px #C4C4C4;
        }
        .judul3{
            font-weight: 200;
            font-size: 22px;
            line-height: 30px;
            text-shadow: 2px 2px 4px #C4C4C4;
        }
    </style>
<body>
    <div class="d-lg-flex justify-content-center">
        <div class="bd-highlight w-100">
            <div class="d-flex flex-column justify-content-center judul px-5" style="height: 100vh; background-color: #A8D1E0;">
                <p class="judul1 fst-italic mb-1">
                    Selamat Datang di
                </p>
                <p class="judul2 fst-italic mb-1">
                    Sistemin Bali
                </p>
                <p class="judul3 text-wrap">
                    Website berbasis Knowledge Based System khusus Provinsi Bali
                </p>
            </div>
        </div>
        <div class="bd-highlight w-100">
            <div class="d-flex flex-column justify-content-center align-items-center" style="height: 100vh; background-color: white;">
                <div class="p-2 mb-2 bd-highlight regis">
                    <p>Register</p>
                </div>
                <form method="POST" action="{{ route('register') }}">
                    @csrf
                    <div class="p-2 bd-highlight">
                        <label class="form-label">Nama</label>
                        <input type="nama" name="nama" class="form-control input-regis" id="exampleInputEmail1">
                    </div>
                    <div class="p-2 bd-highlight">
                        <label class="form-label">Email</label>
                        <input type="email" name="email" class="form-control input-regis" id="exampleInputEmail1">
                    </div>
                    <div class="p-2 bd-highlight">
                        <label class="form-label">Kata Sandi</label>
                        <input type="password" name="password" class="form-control input-regis" id="exampleInputEmail1" >
                    </div>
                    <div class="p-2 mb-3 bd-highlight">
                        <label class="form-label">Konfirmasi Kata Sandi</label>
                        <input type="password" name="confirmpassword" class="form-control input-regis" id="exampleInputEmail1" >
                    </div>
                    <div class="p-2 bd-highlight text-center">
                        <button type="submit" class="btn btn-regis me-2">Register</button>
                        <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                            {{ __('Already registered?') }}
                        </a>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script src="{{ asset('/js/app.js') }}">
    </script>
</body>
</html>






{{-- <x-guest-layout>
    <x-auth-card>
        <x-slot name="logo">
            <a href="/">
                <x-application-logo class="w-20 h-20 fill-current text-gray-500" />
            </a>
        </x-slot>

        <!-- Session Status -->
        <x-auth-session-status class="mb-4" :status="session('status')" />

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('login') }}">
            @csrf

            <!-- Email Address -->
            <div>
                <x-label for="email" :value="__('Email')" />

                <x-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required autofocus />
            </div>

            <!-- Password -->
            <div class="mt-4">
                <x-label for="password" :value="__('Password')" />

                <x-input id="password" class="block mt-1 w-full"
                                type="password"
                                name="password"
                                required autocomplete="current-password" />
            </div>

            <!-- Remember Me -->
            <div class="block mt-4">
                <label for="remember_me" class="inline-flex items-center">
                    <input id="remember_me" type="checkbox" class="rounded border-gray-300 text-indigo-600 shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50" name="remember">
                    <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
                </label>
            </div>

            <div class="flex items-center justify-end mt-4">
                @if (Route::has('password.request'))
                    <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('password.request') }}">
                        {{ __('Forgot your password?') }}
                    </a>
                @endif

                <x-button class="ml-3">
                    {{ __('Log in') }}
                </x-button>
            </div>
        </form>
    </x-auth-card>
</x-guest-layout> --}}
