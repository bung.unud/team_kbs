@extends('landing.layouts.app')
@section('content')
@include('landing.modals.detailGambar')
    <!-- Content search result -->
    <section id="search-result" class="d-flex align-items-start my-3">
        <div class="container" >
            <div class="row">
                <div class="col-lg-8">
                    <form class="input-group mb-3" action="post">
                        <input type="search" class="form-control input-search-result" placeholder="Telusuri website ini" aria-label="Recipient's username" aria-describedby="button-addon2">
                        <span class="p-0 input-group-text line-white-search">|</span>
                        <button class="btn btn-secondary btn-search-result" onclick="location.href='{{ url('/user/not-found') }}'" type="button" id="button-addon2"><i class="bi bi-search"></i></button>
                    </form>
                </div>
            </div>
            <!-- Nav category -->
            <div class="row">
                <div class="col-12">
                    <ul class="nav nav-pills my-4" id="pills-tab" role="tablist">
                        <li class="pe-5 nav-item" role="presentation">
                            <button class="nav-link" onclick="location.href='{{ url('/user/search-results') }}'" type="button" aria-selected="false">
                                Semua
                            </button>
                        </li>
                        <li class="pe-5 nav-item" role="presentation">
                            <button class="nav-link active" onclick="location.href='{{ url('/user/search-results-gambar') }}'" type="button" aria-selected="true">
                                Gambar
                            </button>
                        </li>
                        <li class="pe-5 nav-item" role="presentation">
                            <button class="nav-link" onclick="location.href='{{ url('/user/search-results-video') }}'" type="button" aria-selected="false">
                                Video
                            </button>
                        </li>
                        <li class="pe-5 nav-item" role="presentation">
                            <button class="nav-link" onclick="location.href='{{ url('/user/search-results-document') }}'" type="button" aria-selected="false">
                                Dokumen
                            </button>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="gambar">
                            <div class="d-flex flex-wrap">
                                <div class="card me-3 mt-3" style="width: 16rem;">
                                    <img src="{{ asset('assets/img/gwk-bali.png') }}" class="card-img-top" alt="...">
                                    <div class="card-body d-flex flex-column align-items-end">
                                        <div class="col-12 mb-2">
                                            <h5 class="card-title" style="font-weight: 600; font-size: 14px; line-height: 18px; color:black;">
                                                Garuda Wisnu Kencana
                                            </h5>
                                        </div>
                                        <div class="col-12 mb-3">
                                            <p class="card-text" style="font-weight: 290; font-size: 11px; line-height: 12px; color: black; text-align: justify; ">
                                                Destinasi Wisata Paling Favorit di Bali dengan Patung GWK sebagai Tertinggi Ke Empat di Dunia Menampilkan 15 Pertunjukan Kesenian Bali, Tiap Hari, Tiap Jam.
                                            </p>
                                        </div>
                                        <div class="col-12 mt-auto text-center">
                                            <a href="#" class="btn" data-bs-toggle="modal" data-bs-target="#detailGambar" style="background-color: #00415A">
                                                <i class="bi bi-download me-1" style="color: white; font-size: 1rem;"></i><span style="color: white;">Unduh</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card me-3 mt-3" style="width: 16rem;">
                                    <img src="{{ asset('assets/img/tanah-lot-bali 1.png') }}" class="card-img-top">
                                    <div class="card-body d-flex flex-column align-items-end">
                                        <div class="col-12 mb-2">
                                            <h5 class="card-title" style="font-weight: 600; font-size: 14px; line-height: 18px; color:black;">
                                                Pura Tanah Lot
                                            </h5>
                                        </div>
                                        <div class="col-12 mb-3">
                                            <p class="card-text" style="font-weight: 290; font-size: 11px; line-height: 12px; color: black; text-align: justify; ">
                                                Tanah Lot salah satu pura penting bagi umat Hindu Bali dan lokasi pura terletak di atas batu besar yang berada di lepas pantai.
                                            </p>
                                        </div>
                                        <div class="col-12 mt-auto text-center">
                                            <a href="#" class="btn" style="background-color: #00415A">
                                                <i class="bi bi-download me-1" style="color: white; font-size: 1rem;"></i><span style="color: white;">Unduh</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card me-3 mt-3" style="width: 16rem;">
                                    <img src="{{ asset('assets/img/pura-ulun.png') }}" class="card-img-top" alt="...">
                                    <div class="card-body d-flex flex-column align-items-end">
                                        <div class="col-12 mb-2">
                                            <h5 class="card-title" style="font-weight: 600; font-size: 14px; line-height: 18px; color:black;">
                                                Pura Ulun Danu Bratan
                                            </h5>
                                        </div>
                                        <div class="col-12 mb-3">
                                            <p class="card-text" style="font-weight: 290; font-size: 11px; line-height: 12px; color: black; text-align: justify; ">
                                                Pura Ulun Danu Bratan, Pura Ulun Danu Beratan atau Bratan Pura merupakan sebuah pura dan candi air besar di Bali, Indonesia
                                            </p>
                                        </div>
                                        <div class="col-12 mt-auto text-center">
                                            <a href="#" class="btn" style="background-color: #00415A">
                                                <i class="bi bi-download me-1" style="color: white; font-size: 1rem;"></i><span style="color: white;">Unduh</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card me-3 mt-3" style="width: 16rem;">
                                    <img src="{{ asset('assets/img/pantai-pandawa.png') }}" class="card-img-top" alt="...">
                                    <div class="card-body d-flex flex-column align-items-end">
                                        <div class="col-12 mb-2">
                                            <h5 class="card-title" style="font-weight: 600; font-size: 14px; line-height: 18px; color:black;">
                                                Pantai Pandawa
                                            </h5>
                                        </div>
                                        <div class="col-12 mb-3">
                                            <p class="card-text" style="font-weight: 290; font-size: 11px; line-height: 12px; color: black; text-align: justify; ">
                                                Pantai Pandawa Bali, salah satu pantai pasir putih terkenal di Bali yang wajib di kunjungi jika anda liburan di pulau dewata.
                                            </p>
                                        </div>
                                        <div class="col-12 mt-auto text-center">
                                            <a href="#" class="btn" style="background-color: #00415A">
                                                <i class="bi bi-download me-1" style="color: white; font-size: 1rem;"></i><span style="color: white;">Unduh</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card me-3 mt-3" style="width: 16rem;">
                                    <img src="{{ asset('assets/img/air-terjun-nungnung.png') }}" class="card-img-top" alt="...">
                                    <div class="card-body d-flex flex-column align-items-end">
                                        <div class="col-12 mb-2">
                                            <h5 class="card-title" style="font-weight: 600; font-size: 14px; line-height: 18px; color:black;">
                                                Air Terjun Nungnung
                                            </h5>
                                        </div>
                                        <div class="col-12 mb-3">
                                            <p class="card-text" style="font-weight: 290; font-size: 11px; line-height: 12px; color: black; text-align: justify; ">
                                                Air Terjun Nungnung dikenal sebagai salah satu air terjun tertinggi yang ada di Bali. Nungnung dikenal mempunyai ketinggian mencapai 50 meter.
                                            </p>
                                        </div>
                                        <div class="col-12 mt-auto text-center">
                                            <a href="#" class="btn" style="background-color: #00415A">
                                                <i class="bi bi-download me-1" style="color: white; font-size: 1rem;"></i><span style="color: white;">Unduh</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card me-3 mt-3" style="width: 16rem;">
                                    <img src="{{ asset('assets/img/pantai-kelingking.png') }}" class="card-img-top">
                                    <div class="card-body d-flex flex-column align-items-end">
                                        <div class="col-12 mb-2">
                                            <h5 class="card-title" style="font-weight: 600; font-size: 14px; line-height: 18px; color:black;">
                                                Pantai Kelingking
                                            </h5>
                                        </div>
                                        <div class="col-12 mb-3">
                                            <p class="card-text" style="font-weight: 290; font-size: 11px; line-height: 12px; color: black; text-align: justify; ">
                                                Pantai Kelingking berada di Desa Bunga Mekar, Kecamatan Nusapenida, Kabupaten Klungkung, Bali.  Disebut kelingking karena pintu masuk pantai yang menyerupai kelingking, dengan tebing dan bukitnya yang menjorok ke arah laut. 
                                            </p>
                                        </div>
                                        <div class="col-12 mt-auto text-center">
                                            <a href="#" class="btn" style="background-color: #00415A">
                                                <i class="bi bi-download me-1" style="color: white; font-size: 1rem;"></i><span style="color: white;">Unduh</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card me-3 mt-3" style="width: 16rem;">
                                    <img src="{{ asset('assets/img/pantai-pasih-uug.png') }}" class="card-img-top" alt="...">
                                    <div class="card-body d-flex flex-column align-items-end">
                                        <div class="col-12 mb-2">
                                            <h5 class="card-title" style="font-weight: 600; font-size: 14px; line-height: 18px; color:black;">
                                                Pantai Pasih Uug (Broken Beach)
                                            </h5>
                                        </div>
                                        <div class="col-12 mb-3">
                                            <p class="card-text" style="font-weight: 290; font-size: 11px; line-height: 12px; color: black; text-align: justify; ">
                                                Pantai Pasih Uug atau dikenal dengan nama broken beach merupakan tempat wisata yang terletak di Banjar Sumpang, Desa Bunga Mekar, Kecamatan Nusapenida, Kabupaten Klungkung, Bali, Indonesia. 
                                            </p>
                                        </div>
                                        <div class="col-12 mt-auto text-center">
                                            <a href="#" class="btn" style="background-color: #00415A">
                                                <i class="bi bi-download me-1" style="color: white; font-size: 1rem;"></i><span style="color: white;">Unduh</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card me-3 mt-3" style="width: 16rem;">
                                    <img src="{{ asset('assets/img/bali-botanical-garden.png') }}" class="card-img-top" alt="...">
                                    <div class="card-body d-flex flex-column align-items-end">
                                        <div class="col-12 mb-2">
                                            <h5 class="card-title" style="font-weight: 600; font-size: 14px; line-height: 18px; color:black;">
                                                Bali Botanical Garden
                                            </h5>
                                        </div>
                                        <div class="col-12 mb-3">
                                            <p class="card-text" style="font-weight: 290; font-size: 11px; line-height: 12px; color: black; text-align: justify; ">
                                                Menikmati sejuknya udara Bedugul tentu bakal terasa kian lengkap dengan berjalan-jalan di tengah kebun bunga indah.
                                            </p>
                                        </div>
                                        <div class="col-12 mt-auto text-center">
                                            <a href="#" class="btn" style="background-color: #00415A">
                                                <i class="bi bi-download me-1" style="color: white; font-size: 1rem;"></i><span style="color: white;">Unduh</span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Pagination -->
            <div class="row my-5 d-flex">
                <nav aria-label="pagination" class="d-flex justify-content-center">
                    <ul class="pagination">
                        <li class="p-2 page-item active">
                            <a class="page-link" href="#">1</a>
                        </li>
                        <li class="p-2 page-item">
                            <a class="page-link" href="#">2</a>
                        </li>
                        <li class="p-2 page-item">
                            <a class="page-link" href="#">3</a>
                        </li>
                        <li class="p-2 page-item">
                            <a class="page-link" href="#">4</a>
                        </li>
                        <li class="p-2 page-item">
                            <a class="page-link" href="#">5</a>
                        </li>
                        <li class="p-2 page-item">
                            <a class="page-link" href="#">Berikutnya<i class="bi bi-chevron-right icon-arrow"></i></a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </section>
@endsection