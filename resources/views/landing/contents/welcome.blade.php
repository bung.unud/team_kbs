@extends('landing.layouts.app')

@section('content')
    <!-- Hero section -->
    <section id="hero" class="d-flex align-items-center">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 text-center hero-tagline">
                    <h1 class="mb-2">sistemin bali</h1>
                    <p>Pusat Penyedia Informasi Provinsi Bali</p>
                </div>
                <div class="col-8 mx-auto mt-5">
                    <form method="GET" action="{{ url('/user/search-results') }}" >
                        @csrf
                        <div class="input-group">
                            <input type="text" name="input_search" class="form-control input-search" placeholder="Telusuri website ini">
                            <button class="btn btn-secondary btn-search" type="submit">
                                <i class="bi bi-search"></i>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <!-- List section -->
    <section id="list-questions" class="d-flex align-items-center">
        <div class="container">
            <div class="row">
                <div class="col-12 mb-3 text-center list-questions-tagline">
                    <h3>
                        Daftar Pertanyaan Yang Paling Banyak Dicari
                    </h3>
                </div>
                <div class="col-8 mx-auto">
                    <div class="accordion accordion-flush" id="accordionFlushExample">
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="flush-headingOne">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                                    Bagaimana cara membuat KTP?
                                </button>
                            </h2>
                            <div id="flush-collapseOne" class="accordion-collapse collapse"
                                aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                                <div class="accordion-body">
                                    <p>
                                        1. Mengunduh aplikasi online

                                        Pemohon mengakses aplikasi online DUKCAPIL ONLINE yang tersedia. Simak fitur
                                        yang ditawarkan di halaman website resminya di dukcapil.online.
                                    </p>
                                    <p>
                                        2. Pilih layanan KTP

                                        Pilih jenis layanan KARTU TANDA PENDUDUK.
                                    </p>
                                    <p>
                                        3. Lengkapi data

                                        Mengisi data yang dibutuhkan merupakan bagian dari penyelesaian cara membuat KTP
                                        online.
                                    </p>
                                    <p>
                                        4. Siapkan dan unggah dokumen

                                        Mengunggah dokumen sesuai dengan persyaratan.
                                    </p>
                                    <p>
                                        5. Verifikasi

                                        Menunggu proses verifikasi berkas permohonan sampai mendapatkan notifikasi
                                        status layanan. Cara membuat KTP online selesai.
                                    </p>
                                    <p>
                                        6. Penyerahan KTP baru

                                        Peserta akan diminta untuk menyerahkan KTP lama dan menerima KTP baru di
                                        kecamatan setempat dengan membawa bukti pendaftaran. Akan tetapi bagi peserta
                                        yang belum pernah memiliki KTP sebelumnya, akan langsung diberikan KTP.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="flush-headingTwo">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                                    Bagaimana cara membuat KTP?
                                </button>
                            </h2>
                            <div id="flush-collapseTwo" class="accordion-collapse collapse"
                                aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                                <div class="accordion-body">Lorem Ipsum is simply dummy text of the printing and
                                    typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever
                                    since the 1500s, when an unknown printer took a galley of type and scrambled it to
                                    make a type specimen book. It has survived not only five centuries, but also the
                                    leap into electronic typesetting, remaining essentially unchanged.
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="flush-headingThree">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                                    Bagaimana cara membuat KTP?
                                </button>
                            </h2>
                            <div id="flush-collapseThree" class="accordion-collapse collapse"
                                aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                                <div class="accordion-body">
                                    Lorem Ipsum is simply dummy text of the printing and
                                    typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever
                                    since the 1500s, when an unknown printer took a galley of type and scrambled it to
                                    make a type specimen book. It has survived not only five centuries, but also the
                                    leap into electronic typesetting, remaining essentially unchanged.
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="flush-headingFour">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFour" aria-expanded="false" aria-controls="flush-collapseFour">
                                    Bagaimana cara membuat KTP?
                                </button>
                            </h2>
                            <div id="flush-collapseFour" class="accordion-collapse collapse"
                                aria-labelledby="flush-headingFour" data-bs-parent="#accordionFlushExample">
                                <div class="accordion-body">
                                    Lorem Ipsum is simply dummy text of the printing and
                                    typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever
                                    since the 1500s, when an unknown printer took a galley of type and scrambled it to
                                    make a type specimen book. It has survived not only five centuries, but also the
                                    leap into electronic typesetting, remaining essentially unchanged.
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="flush-headingFive">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFive" aria-expanded="false" aria-controls="flush-collapseFive">
                                  Bagaimana cara membuat KTP?
                              </button>
                            </h2>
                            <div id="flush-collapseFive" class="accordion-collapse collapse"
                                aria-labelledby="flush-headingFive" data-bs-parent="#accordionFlushExample">
                                <div class="accordion-body">Lorem Ipsum is simply dummy text of the printing and
                                    typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever
                                    since the 1500s, when an unknown printer took a galley of type and scrambled it to
                                    make a type specimen book. It has survived not only five centuries, but also the
                                    leap into electronic typesetting, remaining essentially unchanged.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 mt-5 text-center list-questions-tagline">
                    <p>
                        Atau Ajukan Pertanyaan Anda <span data-bs-toggle="modal" data-bs-target="#personalData" style="text-decoration: underline; color:blue; cursor:pointer;">disini</span> 
                    <p>
                </div>
            </div>
        </div>
    </section>
@endsection