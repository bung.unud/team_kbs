@extends('landing.layouts.app')
@section('content')
    <!-- Content search result -->
    <section id="search-result" class="d-flex align-items-start my-3">
        <div class="container" >
            <div class="row">
                <div class="col-lg-8">
                    <form class="input-group mb-3" action="post">
                        <input type="search" class="form-control input-search-result" placeholder="Telusuri website ini" aria-label="Recipient's username" aria-describedby="button-addon2">
                        <span class="p-0 input-group-text line-white-search">|</span>
                        <button class="btn btn-secondary btn-search-result" onclick="location.href='{{ url('/user/not-found') }}'" type="button" id="button-addon2"><i class="bi bi-search"></i></button>
                    </form>
                </div>
            </div>
            <!-- Nav category -->
            <div class="row">
                <div class="col-12">
                    <ul class="nav nav-pills my-4" id="pills-tab" role="tablist">
                        <li class="pe-5 nav-item" role="presentation">
                            <button class="nav-link" onclick="location.href='{{ url('/user/search-results') }}'" type="button" aria-selected="false">
                                Semua
                            </button>
                        </li>
                        <li class="pe-5 nav-item" role="presentation">
                            <button class="nav-link" onclick="location.href='{{ url('/user/search-results-gambar') }}'" type="button" aria-selected="false">
                                Gambar
                            </button>
                        </li>
                        <li class="pe-5 nav-item" role="presentation">
                            <button class="nav-link active" onclick="location.href='{{ url('/user/search-results-video') }}'" type="button" aria-selected="true">
                                Video
                            </button>
                        </li>
                        <li class="pe-5 nav-item" role="presentation">
                            <button class="nav-link" onclick="location.href='{{ url('/user/search-results-document') }}'" type="button" aria-selected="false">
                                Dokumen
                            </button>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="video" role="tabpanel" aria-labelledby="pills-contact-tab">
                            <div class="col-lg-8 tagline-search">
                                <div class="col-lg-8">
                                    <p>
                                        www.youtube.com > watch 
                                    </p>
                                    <a href="https://www.youtube.com/watch/CH92Ut4pYZ0">
                                        <h5>
                                            10 Tempat Wisata di Bali Paling Hits - Youtube
                                        </h5>
                                    </a>
                                </div>
                                <div class="d-flex">
                                    <div class="ratio me-3 flex-shrink-1 bd-highlight" style="width: 292px; height: 165px; border-radius: 15px; margin: 0;">
                                        <iframe src="https://www.youtube.com/embed/CH92Ut4pYZ0?rel=0" title="YouTube video" allowfullscreen></iframe>
                                    </div>
                                    <div class="w-50 d-flex flex-column align-items-start">
                                        <div class="bd-highlight">
                                            <p>
                                                The following is a list of 10 recommendations from the Republic of Tourism. 1. Tanah Lot Temple 2. Kuta Beach 3. Uluwatu...
                                            </p>
                                        </div>
                                        <div class="mt-auto bd-highlight">
                                            <p>
                                                Youtube . Channel Youtube . 25 Maret 2021
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8 tagline-search">
                                <div class="col-lg-8">
                                    <p>
                                        www.youtube.com > watch 
                                    </p>
                                    <a href="https://www.youtube.com/watch/V4QFbuPkZYk">
                                        <h5>
                                            7 Objek Wisata Gianyar Ubud Bali Yang Paling Populer - Youtube
                                        </h5>
                                    </a>
                                </div>
                                <div class="d-flex">
                                    <div class="ratio me-3 flex-shrink-1 bd-highlight" style="width: 292px; height: 165px; border-radius: 15px; margin: 0;">
                                        <iframe src="https://www.youtube.com/embed/V4QFbuPkZYk?rel=0" title="YouTube video" allowfullscreen></iframe>
                                    </div>
                                    <div class="w-50 d-flex flex-column align-items-start">
                                        <div class="bd-highlight">
                                            <p>
                                                Anda tentunya tidak akan melewatkan destinasi gianyar ubud ini jika berkunjung ke pulau BALI. Disini sangat kental akan....
                                            </p>
                                        </div>
                                        <div class="mt-auto bd-highlight">
                                            <p>
                                                Youtube . Channel Youtube . 25 Maret 2021
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-8 tagline-search">
                                <div class="col-lg-8">
                                    <p>
                                        www.youtube.com > watch 
                                    </p>
                                    <a href="https://www.youtube.com/watch/qoea-Nq_v20">
                                        <h5>
                                            5 Tempat Wisata di Bali yang Wajib Dikunjungi di 2022 - Youtube
                                        </h5>
                                    </a>
                                </div>
                                <div class="d-flex">
                                    <div class="ratio me-3 flex-shrink-1 bd-highlight" style="width: 292px; height: 165px; border-radius: 15px; margin: 0;">
                                        <iframe src="https://www.youtube.com/embed/qoea-Nq_v20?rel=0" title="YouTube video" allowfullscreen></iframe>
                                    </div>
                                    <div class="w-50 d-flex flex-column align-items-start">
                                        <div class="bd-highlight">
                                            <p>
                                                Inilah 5 wisata hits di Bali yang wajib anda kunjungi di tahun 2022 diantaranya adalah Garuda Wisnu Kencana, Bedugul, Uluwatu, Kintamani dan Desa Penglipuran.
                                            </p>
                                        </div>
                                        <div class="mt-auto bd-highlight">
                                            <p>
                                                Youtube . Channel Youtube . 25 Maret 2021
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Pagination -->
            <div class="row my-5 d-flex">
                <nav aria-label="pagination" class="d-flex justify-content-center">
                    <ul class="pagination">
                        <li class="p-2 page-item active">
                            <a class="page-link" href="#">1</a>
                        </li>
                        <li class="p-2 page-item">
                            <a class="page-link" href="#">2</a>
                        </li>
                        <li class="p-2 page-item">
                            <a class="page-link" href="#">3</a>
                        </li>
                        <li class="p-2 page-item">
                            <a class="page-link" href="#">4</a>
                        </li>
                        <li class="p-2 page-item">
                            <a class="page-link" href="#">5</a>
                        </li>
                        <li class="p-2 page-item">
                            <a class="page-link" href="#">Berikutnya<i class="bi bi-chevron-right icon-arrow"></i></a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </section>
@endsection