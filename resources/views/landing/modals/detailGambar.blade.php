<style>
    .form-control.personal-data{
        height: 36px;

        border-radius: 10px;
    }
</style>

<!-- Modal personal data -->
<div class="modal fade" id="detailGambar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <form action="post">
                <div class="modal-body">
                    <div class="container col-12">
                        <div class="row">
                            <img src="{{ asset('assets/img/gwk-bali.png') }}" alt="" style="border-radius:5px;">
                            <div class="col-12 mb-2">
                                <h5 class="card-title" style="font-weight: 600; font-size: 14px; line-height: 18px; color:black;">
                                    Garuda Wisnu Kencana
                                </h5>
                            </div>
                            <div class="col-12 mb-3">
                                <p class="card-text" style="font-weight: 290; font-size: 11px; line-height: 12px; color: black; text-align: justify; ">
                                    Destinasi Wisata Paling Favorit di Bali dengan Patung GWK sebagai Tertinggi Ke Empat di Dunia Menampilkan 15 Pertunjukan Kesenian Bali, Tiap Hari, Tiap Jam.
                                </p>
                            </div>
                            <div class="col-12 mt-auto text-center">
                                <a href="#" class="btn" data-bs-toggle="modal" data-bs-target="#detailGambar" style="background-color: #00415A">
                                    <i class="bi bi-download me-1" style="color: white; font-size: 1rem;"></i><span style="color: white;">Unduh</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-center">
                    <button type="button" class="btn btn-send mb-4">Kirim</button>
                </div>
            </form>
        </div>
    </div>
</div>