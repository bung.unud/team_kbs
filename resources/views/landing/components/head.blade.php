<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/app.css') }}">
    <!-- Custome Style -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/custome-style.css') }}">

    <!-- Logo title bar -->
    <link rel="icon" href="{{ asset('/assets/img/logo-bali.png') }}" type="image/x-icon">

    <title>Knowledge Base Systems</title>
</head>